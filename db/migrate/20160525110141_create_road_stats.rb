class CreateRoadStats < ActiveRecord::Migration
  def change
    create_table :road_stats do |t|
      t.string :country_code, null: false
      t.integer :year, null: false
      t.integer :week, null: false
      t.string :kind, null: false
      t.integer :count, null: false
      t.decimal :length, null: false, precision: 20, scale: 3
      t.timestamp :created_at
    end
  end
end
