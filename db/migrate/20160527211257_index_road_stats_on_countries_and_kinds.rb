class IndexRoadStatsOnCountriesAndKinds < ActiveRecord::Migration
  def change
    add_index :road_stats, :country_code
    add_index :road_stats, [:country_code, :kind]
  end
end

# CREATE INDEX "index_road_stats_on_dates" ON "road_stats"  (country_code, kind, (year || '' || lpad(week::text, 2, '0')));
# CREATE INDEX "index_road_stats_on_country_dates" ON "road_stats"  (country_code, (year || '' || lpad(week::text, 2, '0')));
