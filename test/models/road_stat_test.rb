require 'test_helper'

class RoadStatTest < ActiveSupport::TestCase
  def build(year, week, length)
    RoadStat.new(year: year, week: week, length: length, count: length)
  end

  test "aggregation" do
    records = [
      build(2016, 20, 999),
      build(2016, 19, 980),
      build(2016, 18, 960),
      build(2016, 17, 900),
      build(2016, 10, 800),
      build(2016, 05, 700),
      build(2015, 01, 100),
      build(2015, 10, 200),
      build(2015, 20, 300)
    ]

    aggregates = RoadStat.aggregate(records, 'year')
    assert_equal (999 + 980 + 960 + 900 + 800 + 700) / 6, aggregates.detect { |a| a.period_name == '2016' }.length
    assert_equal (999 + 980 + 960 + 900 + 800 + 700) / 6, aggregates.detect { |a| a.period_name == '2016' }.count
    assert_equal (100 + 200 + 300) / 3, aggregates.detect { |a| a.period_name == '2015' }.count

    aggregates = RoadStat.aggregate(records, 'quarter')
    assert_equal (100 + 200) / 2, aggregates.detect { |a| a.period_name == '2015 Q1' }.count
    assert_equal 300, aggregates.detect { |a| a.period_name == '2015 Q2' }.count

    aggregates = RoadStat.aggregate(records, 'week')
    assert_equal 200, aggregates.detect { |a| a.period_name == '2015 W10' }.count
    assert_equal 960, aggregates.detect { |a| a.period_name == '2016 W18' }.count
  end
end
