{extend} = _

Chart.defaults.global.legend.display = false

formatNumber = (val) -> number_format(val)

class AP.ChartView
  constructor: ->
    @charts = []
    $(document).on 'model:updated', @update

    # $('.chart-section').on 'click', '.legend-label', (e) =>
    #   @updateChart $(e.target).closest('.chart-wrapper').data('index'), e.currentTarget.dataset.index

  update: =>
    @unitName = if @model.units is 'miles' then 'mi.' else 'km'
    @updateChart(0)
    @updateChart(1)

  updateChart: (chartIndex, seriesIndex) ->
    if @model.groups
      @updateStackedChart(chartIndex, seriesIndex)
    else
      @updateRegularChart(chartIndex, seriesIndex)

  updateRegularChart: (chartIndex, seriesIndex) ->
    data = @model.processed
    seriesIndex ?= @charts[chartIndex]?.seriesIndex ? 0

    datasets = [
      [
        extend {label: 'Length of Roads', data: data.lengths, yAxisID: "y-axis-1"}, Chart.customColorsIndex[0]
        extend {label: 'Number of Roads', data: data.counts, yAxisID: "y-axis-2"}, Chart.customColorsIndex[1]
      ],
      [
        extend {label: 'Length of Roads Change', data: data.lengthDeltas, yAxisID: "y-axis-1"}, Chart.customColorsIndex[0]
        extend {label: 'Number of Roads Change', data: data.countDeltas, yAxisID: "y-axis-2"}, Chart.customColorsIndex[1]
      ]
    ]

    chartData = labels: data.labels, datasets: datasets[chartIndex]

    $container = $(".chart-wrapper[data-index=#{chartIndex}]")

    @charts[chartIndex]?.destroy()
    @charts[chartIndex] = new Chart $container.find("canvas")[0], type: 'line', data: chartData, options:
      responsive: true
      scales:
        yAxes: [ {
            scaleLabel: {labelString: "Distance, #{@unitName}", display: true}, position: "left", id: "y-axis-1",
            ticks: callback: tickFormatter
          }, {
            scaleLabel: {labelString: 'Count', display: true},
            display: true,
            position: "right",
            id: "y-axis-2",
            ticks: callback: tickFormatter
          } ],
        xAxes: [
          { scaleLabel: { display: true, labelString: 'Time' } },
        ]
      tooltips:
        callbacks:
          label: (item, data) =>
            text = "#{data.datasets[item.datasetIndex].label}: #{formatNumber(item.yLabel)}"
            text += " #{@unitName}" if item.datasetIndex is 0
            text

    @charts[chartIndex].seriesIndex = seriesIndex
    @updateLegend _.pluck(datasets[chartIndex], 'label'), $container.find(".legend"), seriesIndex

  updateStackedChart: (chartIndex, seriesIndex) ->
    if @model.groups.unclassified
      labels = @model.groups.unclassified.map (r) -> r.period_name
    else
      labels = []
      
    Colors.reset()


    datasets = for category, rows of @model.groups
      values = rows.map (r) => @model.convertLength(r.length)
      if chartIndex is 1
        values = values.map AP.Model.extractDeltas
      dataset = {label: appRoadTypeNames[category], data: values}
      extend dataset, Colors.next()
    data = labels: labels, datasets: datasets

    $container = $(".chart-wrapper[data-index=#{chartIndex}]")

    @charts[chartIndex]?.destroy()
    @charts[chartIndex] = new Chart $container.find("canvas")[0], type: 'line', data: data, options:
      responsive: true
      scales:
        yAxes: [
          {
            stacked: true
            scaleLabel: {labelString: "Distance, #{@unitName}", display: true}, position: "left", id: "y-axis-1"
            ticks: callback: tickFormatter
          },
        ],
        xAxes: [ { scaleLabel: { display: true, labelString: 'Time' } } ]
      tooltips:
        # mode: 'label'
        callbacks:
          label: (item, data) =>
            text = "#{data.datasets[item.datasetIndex].label}: #{formatNumber(item.yLabel)}"
            text += " #{@unitName}"
            text

    @charts[chartIndex].seriesIndex = seriesIndex
    # @updateLegend _.pluck(datasets, 'label'), $container.find(".legend"), seriesIndex
    @updateLegend null, $container.find(".legend"), seriesIndex

  updateLegend: (labels, container, selectedIndex) ->
    unless labels
      container.html('')
      return
    container.html(
      labels.map (label, index) ->
        colors = Colors.get(index)
        $('<div>', class: 'legend-label', 'data-index': index).addClass('active' if index == Number(selectedIndex)).html([
          $('<span>', class: 'symbol').css(background: colors.backgroundColor, 'border-color': colors.borderColor),
          $('<span>', class: 'title', text: label)
        ])
    )

  updateAxisLabels: (container, labels) ->
    container.find('.axis-label-x').html(labels.x)
    container.find('.axis-label-y1').html(labels.y1)
    container.find('.axis-label-y2').html(labels.y2)

  tickFormatter = (val, idx, ticks) -> formatNumber(val)

  Colors =
    currentIndex: 0
    reset: -> @currentIndex = 0
    next: -> @get @currentIndex++
    count: -> Chart.customColorsIndex.length
    get: (index) -> Chart.customColorsIndex[ index % @count() ]
