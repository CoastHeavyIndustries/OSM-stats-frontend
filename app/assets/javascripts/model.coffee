dateToString = (date) ->
  return '' unless date
  year = date.getFullYear()
  month = date.getMonth() + 1
  day = date.getDate()
  month = '0' + month if month < 10
  day = '0' + day if day < 10
  "#{year}-#{month}-#{day}"

class AP.Model
  constructor: (data) ->
    @reload(data, trigger: no)

  reload: (data, options = {}) ->
    this[k] = v for k, v of data
    @from = new Date Date.parse(@from) if @from
    @till = new Date Date.parse(@till) if @till
    @getUnits()
    @reprocess(trigger: options.trigger)

  update: (newValues = {}) ->
    @country_code = newValues.country_code if newValues.country_code?
    @kind = newValues.kind if newValues.kind?
    params = {}
    params.from = dateToString(@from)
    params.till = dateToString(@till)
    $.get "/dashboard/#{@country_code}/#{@kind}.js", params

  reprocess: (options = {}) ->
    @convertUnits()
    @buildChartData()
    @trigger() unless options.trigger is no

  convertUnits: ->
    for row in @rows
      row.lengthInCurrentUnits = @convertLength(row.length)

  convertLength: (value) ->
    if @units is 'miles' then value * 1.0 else Math.round(value * 1.609344)

  buildChartData: ->
    @processed = {}
    @processed.labels = @rows.map (stat) -> stat.period_name
    @processed.counts = @rows.map (stat) -> stat.count
    @processed.lengths = @rows.map (stat) -> stat.lengthInCurrentUnits
    @processed.countDeltas = @processed.counts.map AP.Model.extractDeltas
    @processed.lengthDeltas = @processed.lengths.map AP.Model.extractDeltas

  getUnits: ->
    @units = localStorage.units ? 'miles'

  setUnits: (value) ->
    if value and value isnt @units
      localStorage.units = value
      @units = value
      @reprocess()

  trigger: (options = {}) ->
    $(document).trigger 'model:updated'
    $(document).trigger 'model:updated:country' if options.country
    $(document).trigger 'model:updated:kind' if options.kind

  start: ->
    @trigger country: true, kind: true

  @extractDeltas = (value, index, array) -> if index is 0 then 0 else value - array[index - 1]
