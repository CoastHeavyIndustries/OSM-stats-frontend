class AP.DashboardView
  constructor: ->
    @map = new AP.MapView
    @chart = new AP.ChartView

    @model = new AP.Model(appBootstrapData)
    @map.model = @model
    @chart.model = @model

    # $('#countries-menu-toggle').on 'show.bs.dropdown', @showCountriesMenu
    # $('#countries-menu-toggle').on 'hide.bs.dropdown', @hideCountriesMenu

    $('.btn-show-countries-menu').on 'click', @showCountriesMenu
    $('body').click => @hideCountriesMenu() if @menuShown
    $('body').keydown (e) => @hideCountriesMenu() if @menuShown and e.keyCode == 27

    $('.input-group.date').datepicker todayHighlight: true, autoclose: true
    $('.date[data-for=from]').datepicker('setDate', @model.from)
    $('.date[data-for=till]').datepicker('setDate', @model.till)

    $('.units-selector button[data-units]').click @changeUnits
    $(".units-selector button[data-units=#{@model.units}]").addClass('active')

    $('.countries-list, .countries-dd').on 'click', 'a[data-country]', (e) =>
      e.preventDefault(); @model.update country_code: $(e.target).data('country')

    $('.kinds-list').on 'click', 'a[data-kind]', (e) =>
      e.preventDefault(); @model.update kind: $(e.target).data('kind')

    $('.dashboard-filter').submit (e) =>
      e.preventDefault()
      @model.from = $('.date[data-for=from]').datepicker('getDate')
      @model.till = $('.date[data-for=till]').datepicker('getDate')
      @model.update()

    $('#search-form .typeahead').typeahead(
      { hint: true, highlight: true, minLength: 1},
      { name: 'states', source: Autocompleter.substringMatcher(_.keys(appCountriesToCodes)), limit: 10 }
    )

    $('#search-form .typeahead').on 'typeahead:close', @tryToChangeCountry

    $(document).on 'model:updated', @update

    @model.start()

  update: (options = {}) =>
    $('.kinds-list li').removeClass('active').filter("[data-kind='#{@model.kind}']").addClass('active')
    $('.dash-title').text("#{@model.country_name} Roads")
    $('#search-form .typeahead').typeahead('val', '')
    $('.kinds-list li').each (index, li) ->
      kind = li.dataset.kind
      color = Chart.customColorsIndex[index]
      if color
        marker = $(li).find('.marker')
        marker.css 'border-color': color.backgroundColor, 'border-left-color': color.backgroundColor

  tryToChangeCountry: =>
    selectedCountryName = $('.typeahead.tt-input').val()
    countryCode = appCountriesToCodes[selectedCountryName]
    if countryCode
      @model.update country_code: countryCode

  showCountriesMenu: =>
    @menuShown = true
    $main = $('main.container')
    $menu = $('#countries-menu')

    containerWidth = $main.width()
    containerOffset = $main.offset()

    if containerWidth < 500
      $menu.css width: '100%', height: '100%', left: 0, top: 0
    else
      $menu.css width: containerWidth, left: containerOffset.left

    $menu.show()
    return false

  hideCountriesMenu: =>
    $('#countries-menu').hide()
    @menuShown = false

  reload: (response) =>
    {country_code, kind} = response
    history.pushState {country_code, kind}, "OSM #{country_code.toUpperCase()}", "/dashboard/#{country_code}/#{kind}"
    @model.reload(response)

  changeUnits: (e) =>
    $button = $(e.target)
    $button.closest('.btn-group').find('button').removeClass('active')
    $button.addClass('active').blur()
    @model.setUnits $button.data('units')
