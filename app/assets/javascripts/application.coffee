#= require jquery/dist/jquery.min.js
#= require jquery_ujs
#= require underscore/underscore
#  require backbone/backbone
#= require auth0-lock/build/auth0-lock.min
#= require leaflet/dist/leaflet
#= require bootstrap-sass/assets/javascripts/bootstrap-sprockets
#= require bootstrap-datepicker/dist/js/bootstrap-datepicker.min
#= require Chart.min
#= require typeahead.js/dist/typeahead.bundle.min.js
#
#= require_self
#= require_tree

window.AP = {}
window.app = {}

$ ->
  if $(document.body).is('.dashboard--show')
    window.dashboard = new AP.DashboardView
