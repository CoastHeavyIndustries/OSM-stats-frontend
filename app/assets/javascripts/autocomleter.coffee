class @Autocompleter
  @substringMatcher: (strs) ->
    return (q, cb) ->
      matches = []
      substrRegex = new RegExp(q, 'i')
      $.each strs, (i, str) ->
        if substrRegex.test(str)
          matches.push(str)
      cb(matches)
