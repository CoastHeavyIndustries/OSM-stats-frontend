L.Icon.Default.imagePath = "/assets/images"

class AP.MapView
  constructor: ->
    @container = $('#map')
    @map = L.map @container[0], trackResize: true
    @map.addLayer L.tileLayer '//{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
    $(document).on 'model:updated', @update

  update: =>
    return if @currentCountry is @model.country_code

    @currentCountry = @model.country_code
    @map.removeLayer @outline if @outline

    if bounds = @model.bounds
      @map.fitBounds [[bounds.min_latitude, bounds.min_longitude ], [bounds.max_latitude, bounds.max_longitude]]
    else
      @map.setView [0, 0], 5

    $.getJSON "/country-outlines/#{@model.country_code}.json", (response) =>
      # points = response.geometry.coordinates[0].map (pt) -> pt.reverse()
      # @outline =  L.polyline(points, {color: 'red'})
      # @map.addLayer @outline
      @outline = L.geoJson(response)
      @outline.addTo(@map)
