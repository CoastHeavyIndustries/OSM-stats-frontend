Chart.customColors = {
  green0: {
    backgroundColor: "rgba(75,192,192,0.2)",
    borderColor: "rgba(75,192,192,1)",
    pointBorderColor: "rgba(75,192,192,1)",
    pointBackgroundColor: "#fff",
    pointHoverBackgroundColor: "rgba(75,192,192,1)",
    pointHoverBorderColor: "rgba(220,220,220,0.8)"
  },
  red: {
    backgroundColor: "rgba(255,99,132,0.2)",
    borderColor: "rgba(255,99,132,1)",
    hoverBackgroundColor: "rgba(255,99,132,0.4)",
    hoverBorderColor: "rgba(255,99,132,1)"
  },
  blue: {
    backgroundColor: "rgba(151,187,205,0.2)",
    borderColor: "rgba(151,187,205,1)",
    pointBackgroundColor: "rgba(151,187,205,1)",
    pointBorderColor: "#fff",
    pointHoverBackgroundColor: "#fff",
    pointHoverBorderColor: "rgba(151,187,205,0.8)"
  }
}

# Chart.customColorsIndex = [Chart.customColors.blue, Chart.customColors.red, Chart.customColors.green0]
Chart.customColorsIndex = []

addColor = (name, stroke) ->
  bg = stroke.replace('1.0', '0.7')
  pointBg = stroke.replace('1.0', '0.8')
  Chart.customColors[name] = {
    backgroundColor: bg
    borderColor: stroke
    pointBorderColor: stroke
    pointBackgroundColor: pointBg
    pointHoverBackgroundColor: bg
    pointHoverBorderColor: stroke
  }

  Chart.customColorsIndex.push Chart.customColors[name]

addColor 'blue', 'rgba(33, 150, 243, 1.0)'
addColor 'red', 'rgba(244,67,54,1.0)'
addColor 'amber', 'rgba(255, 193, 7, 1.0)'
addColor 'blue_grey', 'rgba(96,125,139,1.0)'
addColor 'brown', 'rgba(121, 85, 72, 1.0)'
addColor 'cyan', 'rgba(0, 188, 212,1.0)'
addColor 'deep_orange', 'rgba(255, 87, 34, 1.0)'
addColor 'deep_purple', 'rgba(103, 58, 183, 1.0)'
addColor 'green', 'rgba(76, 175, 80, 1.0)'
addColor 'indigo', 'rgba(63, 81, 181,1.0)'
addColor 'light_blue', 'rgba(3, 169, 244,1.0)'
addColor 'light_green', 'rgba(139, 195, 74, 1.0)'
addColor 'lime', 'rgba(205, 220, 57, 1.0)'
addColor 'yellow', 'rgba(255, 235, 59,1.0)'
addColor 'grey', 'rgba(158,158,158,1.0)'
addColor 'orange', 'rgba(255,152,0,1.0)'
addColor 'pink', 'rgba(233, 30, 99,1.0)'
addColor 'purple', 'rgba(156,39,176,1.0)'
addColor 'teal', 'rgba(0, 150, 136,1.0)'
