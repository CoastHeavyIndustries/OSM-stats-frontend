module ApplicationHelper
  def controller_css_classes
    classes = []

    controller_css_path = controller.controller_path.sub('/', '--')
    classes << controller_css_path
    classes << "#{controller_css_path}--#{action_name}"

    if controller.controller_path.include?('/')
      classes << controller.controller_path.split('/').first
    end

    classes.join(' ')
  end

  def road_options
    RoadStat::KINDS.map { |key, label| [label, key.to_s] }.sort + [['Total', 'total']]
  end
end
