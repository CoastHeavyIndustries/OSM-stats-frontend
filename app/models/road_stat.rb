class RoadStat < ActiveRecord::Base
  validates :kind, :country_code, :year, :week, presence: true
  validates :count, :length, numericality: true

  KINDS = {
    primary:                "Primary",
    unclassified:           "Unclassified",
    secondary:              "Secondary",
    track:                  "Track",
    service:                "Service",
    motorway:               "Motorway",
    living_street:          "Living street",
    path:                   "Path",
    tertiary:               "Tertiary",
    residential:            "Residential",
    raceway:                "Raceway",
    road:                   "Road",
    secondary_link:         "Secondary link",
    primary_link:           "Primary link",
    one_way:                "One Way Streets",
    turn_restrict:          "Turn Restrictions",
    roads_with_names:       "Roads with Names",
    roads_with_designation: "Roads with Designation",
    roads_with_second_lang: "Roads with Different Languages"
  }

  def period_name
    "'#{year.to_s[2..4]} W#{week + 1}"
  end

  def period_start_date
    @period_start_date ||= Date.commercial(year, week, 7)
  # rescue
  #   logger.error "bad perdio start year = #{year}, week= #{week}"
  #   Date.new
  end

  def self.calculate_period(record_count, max_group_size: 40)
    case record_count
      when 0...max_group_size then 'week'
      when max_group_size...(max_group_size * 4) then 'month'
      when (max_group_size * 4)...(max_group_size * 16) then 'quarter'
      else 'year'
    end
  end

  # periods: year month quarter week
  def self.aggregate(records, period = nil, max_group_size: 40)
    period ||= calculate_period records.count, max_group_size: max_group_size

    # logger.debug "aggregating total:#{records.count} period:#{period}"

    groups = records.group_by { |record| record.period_start_date.send("beginning_of_#{period}") }
    totals = groups.map do |date, records|
      VirtualStat.new(
        records.avg(&:length).floor,
        records.avg(&:count).floor,
        date, period, records
      )
    end
    totals.sort_by!(&:period_key)
    totals
  end

  VirtualStat = Struct.new(:length, :count, :start_date, :period, :records) do
    def period_name
      formats = {year: "%Y", quarter: "%Y Q#{start_date.quarter}", month: "%Y %b", week: "%G W%V"}
      start_date.strftime formats[period.to_sym]
    end

    def period_key
      formats = {year: "%Y", quarter: "%Y Q#{start_date.quarter}", month: "%Y %m", week: "%G W%V"}
      start_date.strftime formats[period.to_sym]
    end

    def as_json(options = {})
      {length: length, count: count, period_name: period_name}
    end

    def inspect
      "#{period_name} (#{start_date}) l=#{length} c=#{count}"
    end
  end
end
