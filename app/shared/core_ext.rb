class Date
  def quarter
    month < 3 ? 1 : (month.to_f / 3).ceil
  end

  def cweek_padded
    cweek > 10 ? cweek.to_s : "0#{cweek}"
  end

  def iso_week
    strftime '%V'
  end

  def iso_year
    strftime '%G'
  end
end

class Array
  def avg(&block)
    return 0 if size == 0
    sum(&block) / size
  end
end

class ISO3166::Country
  def short_name
    names.first
  end

  def bounds
    case alpha2
    when 'AU' then {"min_longitude"=>"112", "max_longitude"=>"159.119444", "min_latitude"=>"-29.472222", "max_latitude"=>"-15.5"}
    when 'CN' then {"min_longitude"=>"72", "max_longitude"=>"135", "min_latitude"=>"48", "max_latitude"=>"13"}
    else data.slice('min_longitude', 'max_longitude', 'min_latitude', 'max_latitude')
    end
  end
end
