class DashboardController < ApplicationController
  before_action do
    @country_code = params[:country_code]
    @country = ISO3166::Country.find_country_by_alpha2(@country_code)
    @road_kind = params[:road_kind] || params[:kind]
    @from = Date.parse(params[:from] || cookies[:from]) rescue nil
    @till = Date.parse(params[:till] || cookies[:till]) rescue nil
    cookies[:from] = { value: @from, path: '/dashboard', expires: 1.week.from_now }
    cookies[:till] = { value: @till, path: '/dashboard', expires: 1.week.from_now }
  end

  def root
  end

  def show
    redirect_to default_path and return unless @country_code && @road_kind

    @appdata = roads_json_data
    @appdata.update bounds: @country.bounds if @country
    @appdata.update from: @from, till: @till
    @appdata.update country_name: ISO3166::Country[@country_code]&.short_name

    respond_to do |format|
      format.html
      format.js { render js: "dashboard.reload(#{@appdata.to_json})" }
      format.json { render json: @appdata }
    end
  end

  protected

  def default_path
    country_dashboard_path(@country_code || 'ht', @road_kind || 'total')
  end

  def roads_json_data
    @road_stats = RoadStat.where('country_code = ?', @country_code)
    @road_stats = @road_stats.where('kind = ?', @road_kind) if @road_kind && @road_kind != 'total'
    @road_stats = @road_stats.where("(year || '' || lpad(week::text, 2, '0')) >= ?", "#{@from.iso_year}#{@from.iso_week}") if @from
    @road_stats = @road_stats.where("(year || '' || lpad(week::text, 2, '0')) <= ?", "#{@till.iso_year}#{@till.iso_week}") if @till

    if @road_kind == 'total'
      @groups = @road_stats.group_by(&:kind)
      max_group_size = @groups.map { |kind, rows| rows.size }.max
      period = RoadStat.calculate_period(max_group_size)
      for kind, rows in @groups
        @groups[kind] = RoadStat.aggregate(rows.to_a, period)
      end
      @groups.delete 'deleted_objects'
      @groups.delete 'modified_objects'
      @groups.delete 'new_objects'
      @groups = @groups.sort_by(&:first).to_h
    end

    @aggregates = RoadStat.aggregate(@road_stats.to_a)

    { country_code: @country_code, kind: @road_kind, rows: @aggregates.as_json, groups: @groups }.as_json
  end
end
