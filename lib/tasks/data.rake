task env: :environment

task import: :env do
  RoadStat.transaction do
    RoadStat.delete_all
    Pathname.glob("db/samples/??.csv") do |path|
      CSV.foreach(path) do |row|
        country_code = path.basename('.csv').to_s
        date_str, kind, count, length = row
        date = Date.parse(date_str)
        year = date.year
        week = date.strftime('%W')
        RoadStat.create! country_code: country_code, year: year, week: week, kind: kind, count: count, length: length
      end
    end
  end
end

# cat all-src.json | jq -c  '[.features[] | {code: .properties.iso_a2, geometry: .geometry}]' > all.json
task country_outlines: :env do
  ISO3166::Country.all.each do |country|
    dest = "public/country-outlines/#{country.alpha2.downcase}.json"
    jq = %<[.features[] | {type: "Feature", properties: {code: .properties.iso_a2}, geometry: .geometry}] | .[] | select(.properties.code=="#{country.alpha2}")>
    sh "cat db/data/country-outlines.json | jq -c '#{jq}' > #{dest}"
    sh "rm #{dest}" if File.size(dest) == 0
  end
end

task analyze_country_outlines: :env do
  data = JSON.load File.read "db/data/country-outlines.json"
  p data['features'].count
end

task :load_heroku_data do
  database = 'OSMStats_development'
  %w(road_stats).each do |table|
    puts %{heroku pg:psql -c "\\copy #{table} to 'db-#{table}.csv' csv"}
    puts %{psql -d #{database} -c "delete from #{table}"}
    puts %{psql -d #{database} -c "\\copy #{table} from 'db-#{table}.csv' csv"}
  end
end
