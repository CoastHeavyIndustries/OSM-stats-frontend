require File.expand_path('../boot', __FILE__)

require 'rails/all'
require 'pp'

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module OSMStats
  class Application < Rails::Application
    # Settings in config/environments/* take precedence over those specified here.
    # Application configuration should go into files in config/initializers
    # -- all .rb files in that directory are automatically loaded.

    # Set Time.zone default to the specified zone and make Active Record auto-convert to this zone.
    # Run "rake -D time" for a list of tasks for finding time zone names. Default is UTC.
    # config.time_zone = 'Central Time (US & Canada)'

    # The default locale is :en and all translations from config/locales/*.rb,yml are auto loaded.
    # config.i18n.load_path += Dir[Rails.root.join('my', 'locales', '*.{rb,yml}').to_s]
    # config.i18n.default_locale = :de

    # Do not swallow errors in after_commit/after_rollback callbacks.
    config.active_record.raise_in_transactional_callbacks = true

    # config.autoload_paths << config.root.join("app/shared")

    config.assets.paths << Rails.root / 'bower_components'
    config.assets.precompile << %r(bootstrap-sass/assets/fonts/bootstrap/[\w-]+\.(?:eot|svg|ttf|woff2?)$)
    config.sass.load_paths << Rails.root / 'bower_components'

    # sass settings for bootstrap-sass
    ::Sass::Script::Value::Number.precision = [8, ::Sass::Script::Value::Number.precision].max

    Slim::Engine.set_options pretty: false

    # auth0
    Rails.application.config.middleware.use OmniAuth::Builder do
      provider :auth0, Rails.application.secrets.oauth0['client_id'], Rails.application.secrets.oauth0['client_secret'], Rails.application.secrets.oauth0['domain'], callback_path: "/auth/callback"
    end

    config.to_prepare do
      require_dependency "core_ext"
    end

    console do
      # require 'console_methods'
      # TOPLEVEL_BINDING.eval('self').extend ConsoleMethods
    end
  end
end
