Rails.application.routes.draw do
  root 'dashboard#root'

  get 'auth/callback' => 'auth#callback'
  get 'auth/logout' => 'auth#logout'

  controller :dashboard do
    get 'dashboard(/:country_code(/:road_kind))' => :show, as: :country_dashboard
  end
end
